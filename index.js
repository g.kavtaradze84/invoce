import 'pretty-console-colors';
import fs from 'fs';
import path from 'path';
import { Writable, Readable } from 'stream';

const __dirname = path.resolve();

var inputFile = fs.createReadStream(`${__dirname}/app.xls`);
var outputFile = fs.createWriteStream(`${__dirname}/app_gzip.xls`);

const origin = Readable.from(inputFile);

origin  
    .pipe(outputFile);

outputFile.on('data', (data) => {
    console.log(`data to ${data.toString()}`);
});

outputFile.on('end', () => {
    console.log('end');
});
