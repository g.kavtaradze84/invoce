import 'pretty-console-colors';
import fs from 'fs';
import path from 'path';

const __dirname = path.resolve();
const file = fs.createWriteStream(`${__dirname}/app.xls`);
const str = Buffer.from('0001', 'base64').toString('hex');
file.write(`Invoice Number ${str}\n`);
file.write('Customer ID: \n');
file.write('Customer Name: \n');
file.write('Product List: \n');
file.write('Price: \n');
file.write('Transportation Fee: \n');
file.write(`Date: ${Date.now()}\n`);
file.end('Signature: ');


file.on('open', () => {
    console.log('open');
});

file.on('ready', () => {
    console.log('ready');
});

file.on('finish', () => {
    console.log('finish');
});

file.on('close', () => {
    console.log('close');
});

file.on('error', (err) => {
    console.log('end: ', err);
});