import fs from 'fs';
import path from 'path';
import http from 'http';
import { createGzip } from 'zlib'
import 'pretty-console-colors';

const __dirname = path.resolve();

const server = http.createServer();

server.listen(3000);

server.on('request', (request, response) => {
    fs.createReadStream(`${__dirname}/app_gzip.xls`)
        .pipe(createGzip())
        .pipe(response);
    
    request.on('end', () => {
        console.log('end');
    });

    request.on('error', (err) => {
        console.log(`Error: ${err}`);
    });
});